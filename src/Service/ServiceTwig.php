<?php
namespace App\Service;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Property;
class ServiceTwig{

    protected $em;
    protected $container;

    public function getHappyMessage()
    {
        $messages=['
        its work'];
        $index = array_rand($messages);
        return $messages[$index];
    }

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    /**
     * @return Query
     */

    public function findAll($request)
    {
        $em =$this->em;
        $container = $this->conatainer;
        $query = $this->createQueryBuilder('p')
            ->where('p.sold = false')
            ->getQuery();
        $paginator = $container->get('knp_paginator');
        $results = $paginator->paginate(
            $query,
            $request->query->get('page',1),
            $this->container->getParameter('page_limit')
        );

        return ($results);


    }

}