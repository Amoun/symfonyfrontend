<?php
namespace App\Controller;


use App\Entity\Property;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Repository\PropertyRepository;
use App\Service\ServiceTwig;
use Doctrine\Common\Persistence\ObjectManager;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use App\services;
use Twig\Environment;



class PropertyController extends AbstractController
{

     private $repository;
     private $em;



     public function __construct(PropertyRepository $repository, ObjectManager $em)
    {
        $this->repository = $repository;
        $this->em = $em;
     }


    /**
     * @Route("/biens", name="propertyIndex")
     * @return Response
     */
    public function index(Request $request,PaginatorInterface $paginator): Response
    {
        /**  $property = new Property();
        $property->setTitle('Mon premierbien')
            ->setPrice(20000)
            ->setRooms(4)
            ->setBedrooms(3)
            ->setDiscription('une petite description')
            ->setSurface(60)
            ->setFloor(4)
            ->setHeat(1)
            ->setCity('Sousse')
            ->setAddress('rue touristique chatt matme')
            ->setPostalCode('4042')
        ;
        $em = $this->getDoctrine()->getManager();
        $em->persist($property);
        $em->flush();

 @var  $repository
 * La premier méthode
        $repository = $this ->getDoctrine() ->getRepository(Property::class);
        dump($repository);
 **/
        //$proprety = $this->repository->findAll();
        //$proprety = $this->repository->findAllVisible();
       // $proprety[0]->setSold(true);
        //$this ->em->flush();

        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class,$search);
        $form->handleRequest($request);

        $paginator  = $this->get('knp_paginator');
        $properties = $paginator->paginate(
            $this->repository->findVisibleQuery($search),
            $request->query->getInt('page',1),2
        );
        //partie d la pagination
        //$properties = $this->repository->findAll($request);

        //dump($properties);
        return $this->render( 'property/index.html.twig', [
                'current_menu' => 'properties',
                'properties' => $properties,
                'form' =>$form->createView()
        ]
        );

    }

    /**
     * @Route("/biens/{slug}-{id}", name="property.show", requirements={"slug": "[a-z0-9\]*"})
     * @param Property $property
     * @return Response
     */
    public function show(Property $property, string $slug): Response
    {

        if ($property->getSlug() !== $slug){
            $this->redirectToRoute('property.show',[
                'id' =>$property->getId(),
                 'slug' =>$property->getSlug()
            ], 301);
        }
     return $this->render( 'property/show.html.twig', [
         'current_menu' => 'properties',
         'property' => $property]);
    }
}