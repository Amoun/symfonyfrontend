<?php
namespace App\Controller;

use App\Repository\PropertyRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use App\services;

class HomeController extends AbstractController
{



    /**
     * @Route("/",name="home")
     * @ param PropertyRepository $repository
     * @return Response
     *
     **/
    public function index(PropertyRepository $repository): Response
    {
        $properties = $repository->findLatest();
       return $this->render('pages/home.html.twig', [
           'properties' => $properties
       ]);
       //return new Response('salut les gens');
    }
}